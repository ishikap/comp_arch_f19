#include <iostream>
#include <vector>

std::vector<int> pht(4,0);
std::vector<int> bht(4,0);
int ghr = 0;

void reinitialize_globals() {
    bht.resize(8,0);
    bht.assign(8,0);
    ghr = 0;
}


bool local_pred(int pc_end, bool actual) {
    // Find prediction
    int pred = bht[pht[pc_end]];
    // Update bht
    bht[pht[pc_end]] = actual ? ++bht[pht[pc_end]] : --bht[pht[pc_end]]; 
    bht[pht[pc_end]] = (bht[pht[pc_end]] < 0) ? 0 : bht[pht[pc_end]]; 
    bht[pht[pc_end]] = (bht[pht[pc_end]] > 3) ? 3 : bht[pht[pc_end]]; 
    // Update pht
    pht[pc_end] = ((actual<<1)|(pht[pc_end]>>1));
    // Return Taken/ Not Taken
    return (pred>>1);
}

bool gshare_pred(int pc_end, bool actual) {
    // Find Prediction
    int pred = bht[ghr^pc_end];
    // Update bht
    bht[ghr^pc_end] = actual ? ++bht[ghr^pc_end] : --bht[ghr^pc_end]; 
    bht[ghr^pc_end] = (bht[ghr^pc_end] < 0) ? 0 : bht[ghr^pc_end]; 
    bht[ghr^pc_end] = (bht[ghr^pc_end] > 3) ? 3 : bht[ghr^pc_end];
    // Update ghr
    //ghr = ((actual<<2)|(ghr>>1));
    ghr = ((ghr<<1)|actual)&7;
    return (pred>>1);
}

int main() {
    int pc_ends_2[] = {2, 3, 1, 3, 1, 2, 1, 2, 3};
    bool outcomes[] = {1, 0, 0, 0, 0, 1, 0, 1, 1};
    
    // PART A
    int correct = 0;
    for(unsigned int i = 0; i < 1000000; ++i) {
        for(unsigned int j = 0; j < 9; ++j) {
            correct += (local_pred(pc_ends_2[j], outcomes[j]) == outcomes[j]) ? 1 : 0;
        }
    }
    std::cout << "PART(A): LOACL PREDICTOR ACCURACY = "<< (double)correct/9000000 * 100 <<" %\n";

    // PART B
    int pc_ends_3[] = {6, 7, 1, 7, 1, 6, 1, 6, 7};
    reinitialize_globals();
    correct = 0;
    for(unsigned int i = 0; i < 1000000; ++i) {
        for(unsigned int j = 0; j < 9; ++j) {
            correct += (gshare_pred(pc_ends_3[j], outcomes[j]) == outcomes[j]) ? 1 : 0;
        }
    }
    std::cout << "PART(B): GSHARE PREDICTOR ACCURACY = "<< (double)correct/9000000 * 100 <<" %\n";
    return 0;
}
